<?php

// Add Header Banner
add_action('acf/init', 'register_acf');
function register_acf() {
  if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
      'key' => 'group_5e331a88a9817',
      'title' => 'Homepage Promo Banner',
      'fields' => array(
        array(
          'key' => 'field_5e331a976943b',
          'label' => 'Header Banner',
          'name' => '',
          'type' => 'tab',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'placement' => 'top',
          'endpoint' => 0,
        ),
        array(
          'key' => 'field_5e331aaa6943c',
          'label' => 'Enable',
          'name' => 'banner_enable',
          'type' => 'select',
          'instructions' => 'Turn Banner On or Off',
          'required' => 1,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'choices' => array(
            'show' => 'On',
            'hide' => 'Off',
          ),
          'default_value' => array(
            0 => 'hide',
          ),
          'allow_null' => 0,
          'multiple' => 0,
          'ui' => 0,
          'return_format' => 'value',
          'ajax' => 0,
          'placeholder' => '',
        ),
        array(
          'key' => 'field_5e331b526943e',
          'label' => 'Offer Text',
          'name' => 'header_banner_promo_title',
          'type' => 'wysiwyg',
          'instructions' => '',
          'required' => 1,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
      ),
      'location' => array(
        array(
          array(
            'param' => 'page_type',
            'operator' => '==',
            'value' => 'front_page',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => 1,
      'description' => '',
    ));

  endif;
}

add_action('wp_after_body', function() {
  $frontpage_id = get_option( 'page_on_front' );
  $promoBannerContent = get_field('header_banner_promo_title', $frontpage_id);
  if($promoBannerContent):
    ?>
    <?php if( get_field('banner_enable', get_option( 'page_on_front' ) ) == 'show') { ?>
      <div id="header-banner">
        <div class="header-banner-title">
          <?=$promoBannerContent?>
        </div>
      </div>
    <?php } ?>

    <script async defer type="text/javascript">
    jQuery(document).ready(function($) {
      function toggleHeaderBanner(mode) {
        if(mode == 'activate') {
          $('#header-banner').addClass('active');
          $('body').addClass('header-banner-active');
        } else {
          $('#header-banner').removeClass('active');
          $('body').removeClass('header-banner-active');
        }
      }

      if ($('#header-banner').length) {
        if(typeof(Storage) !== 'undefined' && sessionStorage.getItem('di_close_header_banner') == 'true') {
          toggleHeaderBanner('deactivate');
        } else {
          toggleHeaderBanner('activate');
        }
      }

      $('.header-banner-close').click(function () {
        if (typeof(Storage) !== 'undefined') {
          sessionStorage.setItem('di_close_header_banner', 'true');
        }
        toggleHeaderBanner('deactivate');
      });
    });
    </script>
  <?php endif;
});
// END Add Header Banner
